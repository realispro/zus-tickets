import {Component, OnInit} from '@angular/core';
import {Ticket} from "../model/ticket";
import {Status} from "../model/status";
import {TicketService} from "../service/ticket.service";

@Component({
  selector: 'app-tickets',
  templateUrl: './tickets.component.html',
  styleUrls: ['../main.component.css']
})
export class TicketsComponent implements OnInit {

  tickets:Ticket[];

  constructor(private ticketService: TicketService) {
  }

  ngOnInit(): void {
    this.ticketService.getTickets()
      .subscribe(tickets=>this.tickets=tickets);
  }




}
