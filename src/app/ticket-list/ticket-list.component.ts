import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Ticket} from "../model/ticket";
import {Status} from "../model/status";

@Component({
  selector: 'app-ticket-list',
  templateUrl: './ticket-list.component.html',
  styleUrls: ['../main.component.css']
})
export class TicketListComponent implements OnInit {

  @Input() ticketList:Ticket[];

  selected:Ticket;

  @Output() selectedEvent = new EventEmitter<Ticket>();

  constructor() { }

  ngOnInit(): void {
  }

  ticketClicked(ticket:Ticket){
    console.log(ticket);
    this.selected = ticket;
    this.selectedEvent.emit(ticket);
  }

  clearSelected(){
    this.selected = null;
  }

  startTicket(){
    this.selected.status = Status.Working;
  }

  closeTicket(){
    this.selected.status = Status.Closed;
  }

}
