import {Directive, ElementRef, Input, OnInit} from '@angular/core';
import {Status} from "../model/status";

@Directive({
  selector: '[statusCheck]'
})
export class StatusCheckDirective implements OnInit{

  @Input() status:Status;


  constructor(private element:ElementRef) {
  }

  ngOnInit(): void {
    this.element.nativeElement.innerText = Status[this.status];
    let backgroundColor = "#888";
    switch (this.status){
      case Status.Open:
        backgroundColor = "#F00";
        break;
      case Status.Working:
        backgroundColor = "#ff8c00";
        break;
    }
    this.element.nativeElement.style.backgroundColor = backgroundColor;
  }

}
