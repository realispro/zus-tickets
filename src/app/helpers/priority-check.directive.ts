import {Directive, ElementRef, Input, OnInit} from '@angular/core';

@Directive({
  selector: '[priorityCheck]'
})
export class PriorityCheckDirective implements OnInit{

  @Input() priority:number;


  constructor(private element: ElementRef) {
  }

  ngOnInit(): void {
    console.log("priority:" + this.priority)
    this.element.nativeElement.innerHTML = this.priority;
    if(this.priority==1){
      this.element.nativeElement.style.backgroundColor = '#F00';
    }
  }

}
