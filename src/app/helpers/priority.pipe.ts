import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'priority'
})
export class PriorityPipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): unknown {
    switch (value){
      case 1:
        return "HIGH";
      case 2:
        return "MEDIUM"
      case 3:
        return "LOW";
      default:
        return "NORMAL";
    }
  }

}
