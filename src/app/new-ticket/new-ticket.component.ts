import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {TicketService} from "../service/ticket.service";
import {Ticket} from "../model/ticket";

@Component({
  selector: 'app-new-ticket',
  templateUrl: './new-ticket.component.html',
  styleUrls: ['../main.component.css']
})
export class NewTicketComponent implements OnInit {

  modelForm: FormGroup;
  ticket: Ticket;

  constructor(private formBuilder: FormBuilder, private ticketService: TicketService) {
  }

  ngOnInit(): void {
    this.modelForm = this.formBuilder.group({
      subject: ["", Validators.required],
      description: "",
      priority: [3, [Validators.pattern("[1-3]"), Validators.required]]
    });

    /*
      new FormGroup({
      subject: new FormControl(),
      description: new FormControl(),
      priority: new FormControl(2)
    });*/
  }

  submitForm(){
    console.log("form data:" + JSON.stringify(this.modelForm.value));
    this.ticketService.addTicket(
      this.modelForm.value.subject,
      this.modelForm.value.description,
      this.modelForm.value.priority
    ).subscribe(ticket=>this.ticket);
  }

  clearContext(){
    this.ticket = null;
    this.modelForm.reset();
  }

}
