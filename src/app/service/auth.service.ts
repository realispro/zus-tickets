import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  // OAuth2
  // JWT -> JSON Web Token

  constructor(private http: HttpClient) { }

  login(username:string, password:string){
      console.log("login: " + username +", " + password);
      this.http.post(
        environment.serverUrl + "/login",
        {
          username: username,
          password: password
        }
        ).subscribe(r=>this.setSession(r));

  }

  private setSession(tokenContainer){
    let token = jwt_decode(tokenContainer.accessToken);

    console.log(JSON.stringify(token));

    localStorage.setItem('id_token', tokenContainer.accessToken);
    localStorage.setItem('username', token.username);
    localStorage.setItem('role', token.role);
    // token.exp !!!


  }

  logout(){
    console.log("logout.");
    localStorage.removeItem("id_token");
    localStorage.removeItem("username");
    localStorage.removeItem("role");
  }

  isLogged():boolean{
    // TODO verify expiration as well !!!
    return localStorage.getItem('id_token')!=null;
  }

  getUsername():string{
    return localStorage.getItem('username');
  }

  getRole():string{
    return localStorage.getItem('role');
  }

  getToken():string{
    return localStorage.getItem('id_token');
  }


}
