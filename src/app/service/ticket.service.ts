import { Injectable } from '@angular/core';
import {Ticket} from "../model/ticket";
import {Status} from "../model/status";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class TicketService {

  headers = {
    "abc": "xyz",
    "Content-Type": "application/json"
  };

  constructor(private http: HttpClient) { }

  getTickets():Observable<Ticket[]>{
    console.log("ticket service: get tickets");


    return this.http.get<Ticket[]>(environment.serverUrl + "/tickets",
      {
        headers: this.headers
      });
  }

  addTicket(subject:string, description:string, priority:number):Observable<Ticket>{
    return this.http.post<Ticket>(environment.serverUrl + "/tickets", {
      subject: subject,
      description: description,
      priority: priority
    },
      {
        headers: this.headers
      });
  }
}
