import {CanActivate, Router} from "@angular/router";
import {AuthService} from "./auth.service";
import {Injectable} from "@angular/core";

@Injectable()
export class MyGuard implements CanActivate{

  constructor(private auth: AuthService, private router:Router) {
  }

  canActivate(): boolean {
    let decision = this.auth.isLogged();
    console.log("MyGuard decision: " + decision);
    if(!decision){
      this.router.navigate(['login']);
    }
    return decision;
  }
}
