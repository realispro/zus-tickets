import { Component, OnInit } from '@angular/core';
import {AuthService} from "../service/auth.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['../main.component.css']
})
export class HeaderComponent implements OnInit {

  appLogo =
    "https://morriscenter.org/wp-content/uploads/2016/09/Tickets-Icon-300x300.png";

  appTitle:string = "Tickets";

  auth: AuthService;

  constructor(auth: AuthService) {
    this.auth = auth;
  }

  ngOnInit(): void {
  }

  logout(){
    this.auth.logout();
  }

}
