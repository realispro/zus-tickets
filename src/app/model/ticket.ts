import {Status} from "./status";

export class Ticket {

  id: number;
  subject: string;
  description: string;
  priority: number;
  status: Status;

  constructor(
    id: number,
    subject: string,
    description: string,
    priority: number,
    status: Status
  ) {
    this.id = id;
    this.subject = subject;
    this.description = description;
    this.priority = priority;
    this.status = status;
  }

}
