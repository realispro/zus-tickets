import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from "@angular/forms";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {RouterModule, Routes} from "@angular/router";
import {ReactiveFormsModule} from "@angular/forms";

import {MainComponent} from './main.component';
import {HeaderComponent} from './header/header.component';
import {TicketsComponent} from './tickets/tickets.component';
import {TicketListComponent} from './ticket-list/ticket-list.component';
import {AboutComponent} from './about/about.component';
import {NewTicketComponent} from './new-ticket/new-ticket.component';
import {LoginComponent} from './login/login.component';

import {PriorityCheckDirective} from './helpers/priority-check.directive';
import {StatusCheckDirective} from './helpers/status-check.directive';
import {PriorityPipe} from './helpers/priority.pipe';

import {MyGuard} from "./service/my-guard";
import {TicketService} from "./service/ticket.service";
import {AuthService} from "./service/auth.service";
import {TokenInterceptor} from "./service/token.interceptor";


const routes: Routes = [
  /*{path: '', redirectTo: 'tickets', pathMatch: 'full'},*/
  {path: '', component: TicketsComponent},
  {path: 'about/:param1', component: AboutComponent},
  {path: 'register', canActivate: [MyGuard], component: NewTicketComponent},
  {path: 'login', component: LoginComponent}
];

@NgModule({
  declarations: [
    MainComponent,
    HeaderComponent,
    TicketsComponent,
    TicketListComponent,
    PriorityCheckDirective,
    PriorityPipe,
    StatusCheckDirective,
    AboutComponent,
    NewTicketComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
    MyGuard,
    TicketService,
    AuthService,
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true }
  ],
  bootstrap: [MainComponent]
})
export class AppModule {
}
