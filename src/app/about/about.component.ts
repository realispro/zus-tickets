import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: [ '../main.component.css']
})
export class AboutComponent implements OnInit {

  value:string;


  constructor(route: ActivatedRoute, private router: Router) {
      route.params.subscribe(params=> this.value=params['param1']);
  }

  ngOnInit(): void {
  }

  goToList(){
    this.router.navigate(['']);
  }

}
